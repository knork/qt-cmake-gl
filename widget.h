//
// Created by knork on 11/13/17.
//

#ifndef QTBASIC_WIDGET_H
#define QTBASIC_WIDGET_H

#include <QWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>

QT_FORWARD_DECLARE_CLASS(QPushButton)
class Widget: public QWidget {
	Q_OBJECT;

public:
	explicit Widget(QWidget *parent = 0);

private slots:
	void handle_button();
	void handle_slider();

private:

	QLayout *mainLayout;
	QPushButton *m_button;
	QSlider *m_slider;
	QWidget *openGL_context;

	void addSlider();

};


#endif //QTBASIC_WIDGET_H
