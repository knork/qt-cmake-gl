cmake_minimum_required(VERSION 3.5)
project(qtBasic)


set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -m64 -std=c++11")

file(GLOB ALL
        *.h
        *.cpp
        )


find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)
#find_package(Qt5Gui REQUIRED)

add_executable(qtBasic ${ALL} )

qt5_use_modules(qtBasic Core Widgets)

target_link_libraries(qtBasic ${Qt5Core_LIBRARIES} ${Qt5Widgets_LIBRARIES} )