//
// Created by knork on 11/13/17.
//

#include <QtWidgets/QVBoxLayout>
#include <iostream>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include "widget.h"
#include "renderwindow.h"

Widget::Widget(QWidget *parent)
		: QWidget(parent) {


	mainLayout = new QVBoxLayout;


	/*
	 * Some Controls
	 *
	 */

	addSlider();


	/*
	 * 	OpenGL context
	 */


	QSurfaceFormat fmt;
	fmt.setProfile(QSurfaceFormat::CoreProfile);
	fmt.setRenderableType(QSurfaceFormat::OpenGL);


//	QGroupBox *Box = new QGroupBox(tr("openGL Window"));
	RenderWindow *renderWindow = new RenderWindow(fmt);

	openGL_context = QWidget::createWindowContainer(renderWindow);

	mainLayout->addWidget(openGL_context);



	setLayout(mainLayout);

	setWindowTitle("OpenGL in Qt setup");

	// render Window

}

void Widget::addSlider() {


	QGroupBox *horizontalBox = new QGroupBox(tr("Some controls"));
	QGridLayout *grid = new QGridLayout;



	QLabel *label = new QLabel("Slider 1:");
	grid->addWidget(label,0,0,1,1);
	m_slider = new QSlider(Qt::Horizontal);
	m_slider->setTickPosition(QSlider::NoTicks);
	m_slider->setTickInterval(10);
	m_slider->setSingleStep(1);
	connect(m_slider,SIGNAL(valueChanged(int)),this,SLOT(handle_slider()));
//
	grid->addWidget(m_slider,0,1,1,1);

	horizontalBox->setLayout(grid);
	mainLayout->addWidget(horizontalBox);

	label = new QLabel("Slider 2:");
	grid->addWidget(label,0,3,1,1);

	m_slider = new QSlider(Qt::Horizontal);
	m_slider->setTickPosition(QSlider::NoTicks);
	m_slider->setTickInterval(10);
	m_slider->setSingleStep(1);
	grid->addWidget(m_slider,0,4,1,1);


}

void Widget::handle_button() {
	std::cout << "button pressed" << std::endl;
}

void Widget::handle_slider() {
	std::cout << m_slider->value() << std::endl;

}
